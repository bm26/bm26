============
BM26 project
============

[![build status](https://gitlab.esrf.fr/BM26/bm26/badges/master/build.svg)](http://BM26.gitlab-pages.esrf.fr/BM26)
[![coverage report](https://gitlab.esrf.fr/BM26/bm26/badges/master/coverage.svg)](http://BM26.gitlab-pages.esrf.fr/bm26/htmlcov)

BM26 software & configuration

Latest documentation from master can be found [here](http://BM26.gitlab-pages.esrf.fr/bm26)
